@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-6 col-md-offset-4">
    <h3>{{ $user->name }}</h3>
    Vartotojas užsiregistravo: {{ $user->created_at }}<br>
    Vartotojo el. pašto adresas: <a href="mailto:{{ $user->email }}" target="_top">{{ $user->email }}</a>

    <h4>Vartotojo įkelti maisto priedai ({{ count($additives) }})</h4>
    @foreach($additives as $add)
        <a href="{{ url('additive/'.$add->id) }}">{{$add->code . ' ' . $add->name }}</a><br>
    @endforeach
    <h4>Vartotojo parašyti komentarai ({{ count($comments) }})</h4>
    @foreach($comments as $comment)
        @foreach($additives as $add)
            @if($comment->additive_id == $add->id)

                <a href="{{ url('additive/'.$add->id) }}">{{$add->code . ' ' . $add->name }}</a> <br>Komentuota: {{ $comment->created_at }}<br>
                <p>{{ $comment->comment }}</p>
                @endif
            @endforeach
    @endforeach
        </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("tr").click(function () {
                window.location.href = $(this).attr('href');
            })
        })

    </script>
@endsection