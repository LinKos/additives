@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-5 col-md-offset-3">
        <form method="post" action="{{ url('edit/saveAdditive/' . $additive->id) }}">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <label for="code">Kodas</label>
                <input type="text" class="form-control" id="conde" name="code" value="{{ $additive->code }}" placeholder="Maisto priedo kodas">
                {{ $errors->first('code') }}
            </fieldset>
            <fieldset class="form-group">
                <label for="name">Pavadinimas</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Pavadinimas" value="{{ $additive->name }}" >
            </fieldset>
            <fieldset class="form-group">
                <label for="function">Funkcija</label>
                {!! Form::select('function', $functions, $additive->function, ['class' => 'form-control']) !!}
            </fieldset>
            <fieldset class="form-group">
                <label for="warning">Perspėjimas</label>
                <textarea class="form-control" id="warning" name="warning" rows="3">{{ $additive->warning }}</textarea>
                {{ $errors->first('warning') }}
            </fieldset>
            <fieldset class="form-group">
                <label for="status">Saugumo lygis</label>
                {!! Form::select('status', $safety, null, ['class' => 'form-control']) !!}
            </fieldset>
            <fieldset class="form-group">
                <label for="uses">Kur naudojamas</label>
                <textarea class="form-control" id="uses" name="uses" rows="3">{{ $additive->uses }}</textarea>
                {{ $errors->first('uses') }}
            </fieldset>
            <fieldset class="form-group">
                <label for="details">Aprašymas</label>
                <textarea class="form-control" id="details" name="details" rows="3">{{ $additive->details }}</textarea>
                {{ $errors->first('details') }}
            </fieldset>
            <button type="submit" class="btn btn-primary">Išsaugoti</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
        })

    </script>
@endsection