@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-12">
    <h3 style="text-align: center">Administratoriaus panelė</h3>
        <br>
    <div class="btn-group-vertical" style="margin: auto;" role="group" aria-label="...">
        <a href="{{ url('admin/users') }}"><button type="button" class="btn btn-default">
            Vartotojų valdymas
        </button>
        </a>
        <a href="{{ url('admin/additives') }}"><button type="button" class="btn btn-default">
                Maisto priedų valdymas
            </button>
        </a>
        <br>

    </div>
        <br><br>
        Naujausias vartotojas:
        <a href="{{ url('user/' . $newestUser->id) }}">{{ $newestUser->name }}</a><br> Užsiregistravo: {{ $newestUser->created_at }}

        <br><br>
        Naujausias maisto priedas:
        <a href="{{ url('additive/' . $newestAdditive->id) }}">{{ $newestAdditive->code . ' ' . $newestAdditive->name }}</a><br> Įkeltas: {{ $newestAdditive->created_at }}

        <br><br>
        Naujausias komentaras:<br>
        „{{ $newestComment->comment }}“<br>
        Komentarą parašė: <a href="{{ url('user/' . $userComment->id) }}">{{ $userComment->name }}</a><br>
        Komentaras parašytas: {{ $newestComment->created_at }}<br>
        Pakomentuotas maisto priedas:  <a href="{{ url('additive/' . $commentAdd->id) }}">{{ $commentAdd->code . ' ' . $commentAdd->name }}</a>

    </div>


@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("tr").click(function () {
                window.location.href = $(this).attr('href');
            })
        })

    </script>
@endsection