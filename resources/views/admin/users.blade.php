@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>El. paštas</th>
            <th>Vardas</th>
            <th>Registracijos data</th>
            <th>Veiksmai</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr style="cursor:pointer;" href="{{ url('user/' . $user->id ) }}">
                <td>{{ $user->id }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->created_at }}</td>
                <td>
                    <a href="{{ url('admin/editUser/' . $user->id) }}"><button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
                    @if($user->id !== Auth::user()->id)<button type="button" class="btn btn-danger" data-id="{{ $user->id }}" data-code="{{ $user->name }}" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-times" aria-hidden="true"></i></button>@endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Vartotojo šalinimas</h4>
                </div>
                <div class="modal-body">
                    <p>Ar norite pašalint vartotoją <span class="code"></span></p>
                    <form action="{{ url('deleteUser') }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" class="id" name="id">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Šalinti</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("tr").click(function () {
                window.location.href = $(this).attr('href');
            })
        })
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var code = button.data('code') // Extract info from data-* attributes
            var id = button.data('id') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.code').text(code)
            modal.find('.id').val(id)
            console.log(id);

        })
    </script>
@endsection