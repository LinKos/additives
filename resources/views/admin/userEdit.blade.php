@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-5 col-md-offset-3">
        <form method="post" action="{{ url('edit/saveUser/' . $user->id) }}">
            {{ csrf_field() }}
            <fieldset class="form-group">
                <label for="email">El. paštas</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" placeholder="El. paštas">
                {{ $errors->first('email') }}
            </fieldset>
            <fieldset class="form-group">
                <label for="name">Vardas</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Vartotojo vardas" value="{{ $user->name }}" >
            </fieldset>

            <button type="submit" class="btn btn-primary">Išsaugoti</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
        })

    </script>
@endsection