@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Kodas</th>
            <th>Pavadinimas</th>
            <th>Funkcija</th>
            <th>Saugumas</th>
            <th>Veiksmai</th>
        </tr>
        </thead>
        <tbody>
        @foreach($additives as $add)
            <tr style="cursor: pointer">
                <td class="clickable" href="{{ url('additive/' . $add->id ) }}"><span @if($add->status == 0)style="padding: 5px; border-radius: 6px; background: greenyellow;"
                                                                                      @elseif($add->status == 2)style="padding: 5px; border-radius: 6px; color:white; background: orange;"
                                                                                      @else style="padding: 5px; border-radius: 6px; color: white; background: red;"@endif
                    >{{ $add->code }}</span></td>
                <td class="clickable" href="{{ url('additive/' . $add->id ) }}">{{ $add->name }}</td>
                <td class="clickable" href="{{ url('additive/' . $add->id ) }}">{{ $functions[$add->function] }}</td>
                <td class="clickable" href="{{ url('additive/' . $add->id ) }}">{{ $safety[$add->status] }}</td>
                <td>
                    <a href="{{ url('admin/editAdditive/' . $add->id) }}"><button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>
                    <button type="button" class="btn btn-danger" data-id="{{ $add->id }}" data-code="{{ $add->code }}" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-times" aria-hidden="true"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Maisto priedo šalinimas</h4>
                </div>
                <div class="modal-body">
                    <p>Ar norite pašalinti maisto priedą <span class="code"></span></p>
                    <form action="{{ url('deleteAdditive') }}" method="post">
                        {{ csrf_field() }}

                        <input type="hidden" class="id" name="id">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Šalinti</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("td.clickable").click(function () {
                window.location.href = $(this).attr('href');
            })
        })

        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var code = button.data('code') // Extract info from data-* attributes
            var id = button.data('id') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.code').text(code)
            modal.find('.id').val(id)
            console.log(id);

        })
    </script>
@endsection