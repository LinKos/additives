@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-6 col-md-offset-3">
    <h1 style="text-align: center"><span @if($additive->status == 0)style="padding: 10px; border-radius: 6px; background: greenyellow;"
                                         @elseif($additive->status == 2)style="padding: 10px; border-radius: 6px; background: orange;"
                                         @else style="padding: 10px; border-radius: 6px; background: red; color:white"@endif
        >{{ $additive->code }}</span></h1>

    <h2 style="text-align: center"> {{ $additive->name }}</h2>
    <h4>Saugumo lygis:</h4>
        {{ $safety[$additive->status] }}
    <p>{{ $additive->description }}</p>
    <h4>Funkcija:</h4>
        {{ $functions[$additive->function] }}
    <h4>Panaudojimas: </h4>
    <p>{{ $additive->uses }}</p>
    <h4>Aprašymas: </h4>
    <p>{{ $additive->details }}</p>
        <h4>Išvada: </h4>
        <p>{{ $additive->warning }}</p>
        @if(count($products) > 0)
        <h4>Produktai, sudėtyje turintys {{ $additive->code }}: </h4>
        @foreach($products[$additive->id] as $prod)

            <a href={{ 'http://79.98.29.141/ktu/products/'. $prod->id}}> {{ $prod->name }}</a><br>
        @endforeach
        @endif
        <hr>
        <h4>{{ $additive->code }} paplitimas maisto produktuose, lyginant su kitais maisto priedais: </h4>
        <canvas id="myChart" width="200" height="200"></canvas>

        <hr>
        <h4 style="text-align: center">Komentarai </h4>
        @foreach($comments as $comment)
            <b>{{ $comment->created_at }}</b><br>
            {{ $users[$comment->user_id] }}<br>
            <p>{{ $comment->comment }}</p>
<br>
        @endforeach
        @if(Auth::check())
        <label for="uses">Naujas komentaras</label>
        <form action="{{ url('savecomment') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $additive->id }}" name="addid">
            <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
            {{ $errors->first('comment') }}
            <br>
        {{ $errors->first('uses') }}


            <div class="g-recaptcha" data-sitekey="6LfY_SATAAAAAIW1iBMnZJPiXrwEiQzz9L_vE0Lp"></div>
            {{ $errors->first('g-recaptcha-response') }}
            <br>
            <button type="submit" class="btn btn-primary">Skelbti</button>

        </form>
            @endif
    </div>
@endsection
@section('scripts')
    <script>
        var lbls = <?php echo $counted ?>;
        var inverted = <?php echo $inverted ?>;
        window.onload = function() {
            var data = {
                labels: Object.keys(lbls),

                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: inverted,
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                                "#9966ff",
                                "#00cc00"

                        ]
                    }

                ]
            };

            var ctx = document.getElementById("myChart").getContext("2d");
            var myBarChart = new Chart(ctx, {
                type: 'pie',
                data: data,
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,

                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,

                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0, 0, 0, .05)",

                //Number - Width of the grid lines
                scaleGridLineWidth: 1,

                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,

                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,

                //Boolean - If there is a stroke on each bar
                barShowStroke: true,

                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,

                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,

                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,

                //String - A legend template

            });
        }

    </script>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.min.js"></script>
