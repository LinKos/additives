@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Kodas</th>
            <th>Pavadinimas</th>
            <th>Funkcija</th>
            <th>Saugumas</th>
        </tr>
        </thead>
        <tbody>
        @if(count($additives) > 0)
            @foreach($additives as $add)
            <tr style="cursor:pointer;" href="{{ url('additive/' . $add->id ) }}">
                <td><span @if($add->status == 0)style="padding: 5px; border-radius: 6px; background: greenyellow;"
                          @elseif($add->status == 2)style="padding: 5px; border-radius: 6px; color:white; background: orange;"
                          @else style="padding: 5px; border-radius: 6px; color: white; background: red;"@endif
                    >{{ $add->code }}</span></td>
                <td>{{ $add->name }}</td>
                <td>{{ $functions[$add->function] }}</td>
                <td>{{ $safety[$add->status] }}</td>
            </tr>
            @endforeach
            @else
        Maisto priedų nerasta.
            @endif
        </tbody>
    </table>

@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $("tr").click(function () {
                window.location.href = $(this).attr('href');
            })
        })

    </script>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.min.js"></script>
