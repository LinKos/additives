<html>
<head>
    <title>E - priedai.lt - @yield('title')</title>
    <link rel='stylesheet' href="{{ asset('css/bootstrap.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type='text/css' media='all' />
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<style>
    .footer, .push {
        height: 155px; /* .push must be the same height as .footer */
    }

</style>
</head>
<body style="font-family: 'Droid Sans', sans-serif;">
@section('menu')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">E - priedai</a>
            </div>
            <ul class="nav navbar-nav">
                <li @if(Request::url() === url('additiveslist'))class="active" @endif><a href="{{ url('additiveslist') }}">Visi priedai</a></li>
                <li @if(Request::url() === url('search'))class="active" @endif><a href="{{ url('search') }}">Maisto priedo paieška</a></li>
                @if(Auth::check())
                <li @if(Request::url() === url('newAdditive'))class="active" @endif><a href="{{ url('newAdditive') }}">Pridėti priedą</a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::check())
                    <li @if(Request::url() === url('auth/login'))class="active" @endif><a href="{{ url('auth/login') }}">Prisijungti</a></li>
                    <li @if(Request::url() === url('auth/register'))class="active" @endif><a href="{{ url('auth/register') }}">Registruotis</a></li>
                @else
                    <li><a href="{{ url('auth/logout') }}">Atsijungti</a></li>
                    @if(Auth::user()->type == 1)
                        <li><a href="{{ url('admin') }}">Administratoriaus panelė</a></li>
                        @endif
                    @endif
            </ul>
        </div>
    </nav>
@show

<div class="container">

    @yield('content')
</div>
<script type='text/javascript' src='https://code.jquery.com/jquery-1.12.3.min.js'></script>
<script type='text/javascript' src='{{ asset('js/bootstrap.min.js') }}'></script>
@yield('scripts')


</body>
</html>