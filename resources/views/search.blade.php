@extends('master')

@section('title', 'Page Title')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="col-md-4 col-md-offset-4">
        <h4 style="text-align: center">Maisto priedo paieška</h4>
        <form role="search" method="post" action="{{ url('search') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <fieldset class="form-group">
                    <input type="text" class="form-control" id="code" name="code" placeholder="Maisto priedo kodas">
                    {{ $errors->first('code') }}
                </fieldset>
            </div>
            <button type="submit" class="btn btn-default pull-right" style="text-align: center">Ieškoti <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
    </script>
@endsection
