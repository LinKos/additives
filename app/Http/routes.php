<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses' => 'AdditiveController@index'
]);

Route::get('/additives', [
    'uses' => 'AdditiveController@all'
]);

Route::get('/functions', [
    'uses' => 'AdditiveController@functAll'
]);


Route::get('admin/users', [
    'uses' => 'AdminController@indexUsers'
]);

Route::get('admin/additives', [
    'uses' => 'AdminController@indexAdditives'
]);

Route::get('admin/editAdditive/{id}', [
    'uses' => 'AdminController@edit'
]);

Route::get('admin/editUser/{id}', [
    'uses' => 'AdminController@editUser'
]);

Route::get('admin', [
    'uses' => 'AdminController@index'
]);

Route::get('search', [
    'uses' => 'AdditiveController@search'
]);

Route::post('search', [
    'uses' => 'AdditiveController@searchPost'
]);


Route::get('user/{id}', [
    'uses' => 'AdminController@showUser'
]);

Route::get('additiveslist', [
    'uses' => 'AdditiveController@index'
]);

Route::get('newAdditive', [
    'uses' => 'AdditiveController@create'
]);

Route::post('edit/saveAdditive/{id}', [
    'as' => 'saveAdditive',
    'uses' => 'AdminController@update'
]);

Route::post('edit/saveUser/{id}', [
    'as' => 'saveUser',
    'uses' => 'AdminController@updateUser'
]);



Route::post('savecomment', [
    'as' => 'saveComment',
    'uses' => 'AdditiveController@saveComment'
]);

Route::post('deleteAdditive', [
    'as' => 'deleteAdditive',
    'uses' => 'AdminController@deleteAdditive'
]);

Route::post('deleteUser', [
    'as' => 'deleteUser',
    'uses' => 'AdminController@deleteUser'
]);


Route::post('saveAdditive', [
    'as' => 'saveAdditive',
    'uses' => 'AdditiveController@store'
]);

Route::get('additive/{id}', [
    'uses' => 'AdditiveController@show'
]);

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@logout');