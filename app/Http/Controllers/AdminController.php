<?php

namespace App\Http\Controllers;

use App\Additive;
use App\Comment;
use App\Funct;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexUsers()
    {
        $additives = Additive::groupBy('user_id')->count();
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function indexAdditives()
    {
        $additives = Additive::all();
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $functions = Funct::lists('function', 'id');
        return view('admin.additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);
    }

    public function index()
    {
        $newestUser = User::orderBy('created_at', 'desc')->first();
        $newestAdditive = Additive::orderBy('created_at', 'desc')->first();
        $userAdd = User::find($newestAdditive->user_id);
        $newestComment = Comment::orderBy('created_at', 'desc')->first();
        $userComment = User::find($newestComment->user_id);
        $commentAdd = Additive::find($newestComment->additive_id);
        return view('admin.index', ['newestUser' => $newestUser, 'newestAdditive' => $newestAdditive, 'userAdd' => $userAdd,
            'newestComment' => $newestComment, 'userComment' => $userComment, 'commentAdd' => $commentAdd]);
    }

    public function showUser($id)
    {
        $user = User::find($id);
        $additives = Additive::where('user_id', $user->id)->get();
        $comments = Comment::where('user_id', $user->id)->get();
        return view('admin.showUser', ['user' => $user, 'additives' => $additives, 'comments' => $comments]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $additive = Additive::find($id);
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $functions = Funct::lists('function', 'id');
        return view('admin.additiveEdit', ['additive' => $additive, 'functions' => $functions, 'safety' => $safety]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $additive = Additive::find($id);
        $additive->code = $request->code;
        $additive->name = $request->name;
        $additive->function = $request->function;
        $additive->warning = $request->warning;
        $additive->status = $request->status;
        $additive->uses = $request->uses;
        $additive->details = $request->details;
        $additive->save();
        $additives = Additive::all();
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $functions = Funct::lists('function', 'id');
        return view('admin.additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);
    }

    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;

        $user->save();
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAdditive()
    {
        Additive::destroy((Input::get('id')));
        $additives = Additive::all();
        return view('admin.additives', ['additives' => $additives]);
    }

    public function deleteUser()
    {
        User::destroy((Input::get('id')));
        $users = User::all();
        return view('admin.users', ['users' => $users]);
    }

    public function editUser($id)
    {
        $user = User::find($id);
        return view('admin.userEdit', ['user' => $user]);
    }

}
