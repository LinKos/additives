<?php

namespace App\Http\Controllers;

use App\Additive;
use App\Comment;
use App\Funct;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdditiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $additives = Additive::all();
        $functions = Funct::lists('function', 'id');

        return view('additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);
    }

    public function all()
    {
        return response()->json(Additive::all());
    }

    public function functAll()
    {
        return response()->json(Funct::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $functions = Funct::lists('function', 'id');
        return view('create', ['functions' => $functions, 'safety' => $safety]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:additives|max:10',
            'warning' => 'required',
            'uses' => 'required',
            'details' => 'required',
            'g-recaptcha-response' => 'required',
        ]);
        $niceNames = array(
            'code' => 'Kodo',
            'warning' => 'Perspėjimo',
            'uses' => 'Panaudojimo',
            'details' => 'Aprašymo',
            'g-recaptcha-response' => 'reCAPTCHA'
        );

        $validator->setAttributeNames($niceNames);
        if ($validator->fails()) {
            return redirect('newAdditive')
                ->withErrors($validator)
                ->withInput();
        }
        $additive = Additive::create($request->all());
        $additive->user_id = Auth::user()->id;
        $additive->save();
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $additives = Additive::all();
        $functions = Funct::lists('function', 'id');
        return view('additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = [];
        $counted = [];
        function get_http_response_code($url) {
            $headers = get_headers($url);
            return substr($headers[0], 9, 3);
        }
        $additives = Additive::all();
        foreach($additives as $add) {
            if (get_http_response_code('http://79.98.29.141/ktu/api/v1/products/' . $add->id) == "200") {
                $products[$add->id] = json_decode(file_get_contents('http://79.98.29.141/ktu/api/v1/products/' . $add->id));
                $counted[$add->code] = count(file_get_contents('http://79.98.29.141/ktu/api/v1/products/' . $add->id));
                    }
            }


        $counted = [];
        $inverted = [];
        foreach(array_filter($products) as $key=>$value) {
            $add = Additive::find($key);
            $counted[$add->code] = count($value);

        }
        $inverted = array_values($counted);

        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $functions = Funct::lists('function', 'id');
        $additive = Additive::find($id);
        $users = User::lists('name','id');
        $comments = Comment::where('additive_id', $id)->get();
        return view('additiveShow', ['inverted' => json_encode($inverted), 'counted' => json_encode($counted), 'products' => $products, 'additive' => $additive, 'comments' => $comments, 'users' => $users, 'functions' => $functions, 'safety' => $safety]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveComment(Request $request)
    {
        //if("" !== Input::get('g-recaptcha-response'))
            $validator = Validator::make($request->all(), [
                'g-recaptcha-response' => 'required',
                'comment' => 'required',
            ]);
        $niceNames = array(
            'g-recaptcha-response' => 'reCAPTCHA',
            'comment' => 'Komentaro',
        );

        $validator->setAttributeNames($niceNames);
        if ($validator->fails()) {
            return redirect('additive/'. Input::get('addid'))
                ->withErrors($validator)
                ->withInput();
        }

        $comment = new Comment();
        $comment->additive_id = Input::get('addid');
        $comment->user_id = Auth::user()->id;
        $comment->comment = Input::get('comment');
        $comment->save();
        return redirect('additive/'. Input::get('addid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function search()
    {
        return view('search');
    }

    public function searchPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);
        $niceNames = array(
            'code' => 'Kodo',
        );

        $validator->setAttributeNames($niceNames);
        if ($validator->fails()) {
            return redirect('search')
                ->withErrors($validator)
                ->withInput();
        }

        $additives = Additive::where('code', 'LIKE', Input::get('code') . '%')->get();

        if ($additives !== null) {
            $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
            $functions = Funct::lists('function', 'id');

            foreach ($additives as $add)
                return view('additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);

            $products = [];
            $counted = [];
            function get_http_response_code($url)
            {
                $headers = get_headers($url);
                return substr($headers[0], 9, 3);
            }

            $additives = Additive::all();
            foreach ($additives as $add) {
                if (get_http_response_code('http://79.98.29.141/ktu/api/v1/products/' . $add->id) == "200") {
                    $products[$add->id] = json_decode(file_get_contents('http://79.98.29.141/ktu/api/v1/products/' . $add->id));
                    $counted[$add->code] = count(file_get_contents('http://79.98.29.141/ktu/api/v1/products/' . $add->id));
                }
            }
        }
        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
        $additives = Additive::all();
        $functions = Funct::lists('function', 'id');

        return view('additives', ['additives' => $additives, 'functions' => $functions, 'safety' => $safety]);
    }


//
//        $counted = [];
//        $inverted = [];
//        foreach(array_filter($products) as $key=>$value) {
//            $add = Additive::find($key);
//            $counted[$add->code] = count($value);
//
//        }
//        $inverted = array_values($counted);
//
//        $safety = ['0' => 'Saugus', '1' => 'Pavojingas', '2' => 'Reikėtų vengti'];
//        $functions = Funct::lists('function', 'id');
//        $users = User::lists('name','id');
//        $comments = Comment::where('additive_id', $additive->id)->get();
//        return view('additiveShow', ['inverted' => json_encode($inverted), 'counted' => json_encode($counted), 'products' => $products, 'additive' => $additive, 'comments' => $comments, 'users' => $users, 'functions' => $functions, 'safety' => $safety]);
//    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
