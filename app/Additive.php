<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additive extends Model
{
    protected $table = 'additives';
    protected $fillable = [
        'code', 'name', 'function','description','uses','safe', 'details','warning'
    ];
}
